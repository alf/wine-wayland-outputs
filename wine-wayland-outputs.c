#include <stdbool.h>
#include <wayland-client.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define BOOL bool
#define FALSE false
#define TRUE true

struct wayland_output_mode
{
    int width;
    int height;
};

struct wayland_output
{
    const char *name;
    int logical_x, logical_y;
    int logical_w, logical_h;
    struct wayland_output_mode *current_mode;
};

struct output_info
{
    int x, y;
    struct wayland_output *output;
};

static int output_info_cmp_primary_x_y(const void *va, const void *vb)
{
    const struct output_info *a = va;
    const struct output_info *b = vb;
    BOOL a_is_primary = a->x == 0 && a->y == 0;
    BOOL b_is_primary = b->x == 0 && b->y == 0;

    if (a_is_primary && !b_is_primary) return -1;
    if (!a_is_primary && b_is_primary) return 1;
    if (a->x < b->x) return -1;
    if (a->x > b->x) return 1;
    if (a->y < b->y) return -1;
    if (a->y > b->y) return 1;
    return 0;
}

static inline BOOL output_info_overlap(struct output_info *a, struct output_info *b)
{
    return b->x < a->x + a->output->current_mode->width &&
           b->x + b->output->current_mode->width > a->x &&
           b->y < a->y + a->output->current_mode->height &&
           b->y + b->output->current_mode->height > a->y;
}

/* Map a point to one of the four quadrants of our 2d coordinate space:
 * 0: bottom right (x >= 0, y >= 0)
 * 1: top right (x >= 0, y < 0)
 * 2: bottom left (x < 0, y >= 0)
 * 3: top left (x < 0, y < 0) */
static inline int point_to_quadrant(int x, int y)
{
    return (x < 0) * 2 + (y < 0);
}

/* Decide which of two outputs to keep stationary in order
 * to resolve an overlap. */
static struct output_info *output_info_get_overlap_anchor(struct output_info *a,
                                                          struct output_info *b)
{
    /* Preferences for the direction of growth in each quadrant, with a
     * lower value signifying a higher preference. */
    static const int quadrant_prefs[4][4] =
    {
        {0, 1, 2, 3}, /* quadrant 0 */
        {3, 0, 2, 1}, /* quadrant 1 */
        {2, 3, 0, 1}, /* quadrant 2 */
        {3, 2, 1, 0}, /* quadrant 3 */
    };
    int qa = point_to_quadrant(a->output->logical_x, a->output->logical_y);
    int qb = point_to_quadrant(b->output->logical_x, b->output->logical_y);
    /* Direction of growth if a is the anchor. */
    int qab = point_to_quadrant(b->output->logical_x - a->output->logical_x,
                                b->output->logical_y - a->output->logical_y);
    /* Direction of growth if b is the anchor. */
    int qba = point_to_quadrant(a->output->logical_x - b->output->logical_x,
                                a->output->logical_y - b->output->logical_y);

    /* If the two output origins are in different quadrants, use the output
     * in the lower valued quadrant as the anchor (so effectively outputs
     * grow/move away from quadrant 0). */
    if (qa != qb) return (qa < qb) ? a : b;

    /* If the outputs are in the same quadrant, use the preference for the
     * direction of growth in that quadrant to select the anchor. Again the
     * intended effect is to grow/move outputs away from the origin. */
    return (quadrant_prefs[qa][qab] < quadrant_prefs[qa][qba]) ? a : b;
}

static BOOL output_info_array_resolve_overlaps(struct wl_array *output_info_array)
{
    struct output_info *a, *b;
    BOOL found_overlap = FALSE;

    wl_array_for_each(a, output_info_array)
    {
        wl_array_for_each(b, output_info_array)
        {
            struct output_info *anchor, *move;
            BOOL x_use_end, y_use_end;
            double rel_x, rel_y;

            /* Break if we reach the same output in the inner loop, so that we
             * don't process output pairs twice (since order doesn't matter for
             * our algorithm.) */
            if (a == b) break;

            if (!output_info_overlap(a, b)) continue;
            found_overlap = TRUE;

            /* Decide which output to move to resolve the overlap. */
            anchor = output_info_get_overlap_anchor(a, b);
            move = anchor == a ? b : a;

            /* Move the selected output on the X axis to resolve the overlap,
             * while maintaining the same relative positioning of the outputs as
             * the one they have in logical space. Use either the start or end
             * of the moved output as the point to maintain the relative
             * position of, depending on whether the anchor is before or after
             * the moved output on the axis. */
            x_use_end = move->output->logical_x < anchor->output->logical_x;
            rel_x = (move->output->logical_x - anchor->output->logical_x +
                     (x_use_end ? move->output->logical_w : 0)) /
                    (double)anchor->output->logical_w;
            move->x = anchor->x + anchor->output->current_mode->width * rel_x -
                      (x_use_end ? move->output->current_mode->width : 0);

            /* Similarly for the Y axis. */
            y_use_end = move->output->logical_y < anchor->output->logical_y;
            rel_y = (move->output->logical_y - anchor->output->logical_y +
                     (y_use_end ? move->output->logical_h : 0)) /
                    (double)anchor->output->logical_h;
            move->y = anchor->y + anchor->output->current_mode->height * rel_y -
                      (y_use_end ? move->output->current_mode->height : 0);
        }
    }

    return found_overlap;
}

static void output_info_array_arrange_physical_coords(struct wl_array *output_info_array)
{
    struct output_info *info;
    size_t num_outputs = output_info_array->size / sizeof(struct output_info);
    int steps = 0;

    /* Set the initial physical pixel coordinates. */
    wl_array_for_each(info, output_info_array)
    {
        info->x = info->output->logical_x;
        info->y = info->output->logical_y;
    }

    /* Try to iteratively resolve overlaps, but be defensive and set an upper
     * iteration bound to ensure we avoid infinite loops. */
    while (output_info_array_resolve_overlaps(output_info_array) &&
           ++steps < num_outputs)
        continue;

    /* Now that we have our physical pixel coordinates, sort from physical left
     * to right, but ensure the primary output is first. */
    qsort(output_info_array->data, num_outputs, sizeof(struct output_info),
          output_info_cmp_primary_x_y);
}

typedef struct rect_s
{
    int x, y, w, h;
} rect_t;


struct test
{
    const char *name;
    struct wl_array outputs;
    struct wl_array results;
};

void test_init(struct test *t, const char *name)
{
    t->name = name;
    wl_array_init(&t->outputs);
    wl_array_init(&t->results);
    fprintf(stderr, "Running %s...\n", name);
}

void test_add_output(struct test *t, const char *name, rect_t l, int w, int h)
{
    struct output_info *info = wl_array_add(&t->outputs, sizeof(*info));
    info->output = calloc(1, sizeof(*info->output));
    info->output->current_mode = calloc(1, sizeof(*info->output->current_mode));
    info->output->name = name;

    info->output->logical_x = l.x;
    info->output->logical_y = l.y;
    info->output->logical_w = l.w;
    info->output->logical_h = l.h;
    info->output->current_mode->width = w;
    info->output->current_mode->height = h;
}

static void test_run(struct test *t)
{
    if (t->results.size) return;

    wl_array_copy(&t->results, &t->outputs);
    output_info_array_arrange_physical_coords(&t->results);
}

bool has_error = FALSE;

static void test_verify(struct test *t, const char *name, rect_t phys, int pos)
{
    struct output_info *info;
    int i = 0;

    test_run(t);

    wl_array_for_each(info, &t->results)
    {
        if (strcmp(info->output->name, name)) { i++; continue;}
        if (info->x != phys.x || info->y != phys.y ||
            info->output->current_mode->width != phys.w ||
            info->output->current_mode->height != phys.h ||
            (pos >= 0 && pos != i))
        {
            fprintf(stderr, "ERROR: %s: %s: expected=%d,%d+%dx%d@%d actual=%d,%d+%dx%d@%d\n",
                    t->name, name,
                    phys.x, phys.y, phys.w, phys.h, pos,
                    info->x, info->y, info->output->current_mode->width,
                    info->output->current_mode->height, i);
            has_error = TRUE;

        }
        goto out;
    }


    fprintf(stderr, "FAIL: %s: not found\n", name);

out:
    return;
}

void test_deinit(struct test *t)
{
    struct output_info *info;

    wl_array_release(&t->results);

    wl_array_for_each(info, &t->outputs)
    {
        free(info->output->current_mode);
        free(info->output);
    }

    wl_array_release(&t->outputs);
}

static void test_line_no_scale()
{
    struct test t;
    test_init(&t, __FUNCTION__);

    test_add_output(&t, "A", (rect_t){0, 0, 100, 100}, 100, 100);
    test_add_output(&t, "B", (rect_t){100, 0, 50, 50}, 50, 50);
    test_add_output(&t, "C", (rect_t){-50, 0, 50, 50}, 50, 50);

    test_verify(&t, "A", (rect_t){0, 0, 100, 100}, 0);
    test_verify(&t, "C", (rect_t){-50, 0, 50, 50}, 1);
    test_verify(&t, "B", (rect_t){100, 0, 50, 50}, 2);

    test_deinit(&t);
}

static void test_line_scale()
{
    struct test t;
    test_init(&t, __FUNCTION__);

    test_add_output(&t, "A", (rect_t){0, 0, 100, 100}, 100, 100);
    test_add_output(&t, "B", (rect_t){100, 0, 50, 50}, 100, 100);
    test_add_output(&t, "C", (rect_t){-50, 0, 50, 50}, 100, 100);

    test_verify(&t, "A", (rect_t){0, 0, 100, 100}, 0);
    test_verify(&t, "C", (rect_t){-100, 0, 100, 100}, 1);
    test_verify(&t, "B", (rect_t){100, 0, 100, 100}, 2);

    test_deinit(&t);
}

static void test_line_scale1()
{
    struct test t;
    test_init(&t, __FUNCTION__);

    test_add_output(&t, "A", (rect_t){0, 0, 50, 50}, 100, 100);
    test_add_output(&t, "B", (rect_t){50, 0, 100, 100}, 100, 100);

    test_verify(&t, "A", (rect_t){0, 0, 100, 100}, 0);
    test_verify(&t, "B", (rect_t){100, 0, 100, 100}, 1);

    test_deinit(&t);
}

static void test_line_scale_x()
{
    struct test t;
    test_init(&t, __FUNCTION__);

    test_add_output(&t, "A", (rect_t){0, 0, 50, 50}, 100, 100);
    test_add_output(&t, "B", (rect_t){50, 0, 50, 50}, 100, 100);
    test_add_output(&t, "C", (rect_t){100, 0, 50, 50}, 100, 100);
    test_add_output(&t, "D", (rect_t){150, 0, 50, 50}, 100, 100);

    test_verify(&t, "A", (rect_t){0, 0, 100, 100}, 0);
    test_verify(&t, "B", (rect_t){100, 0, 100, 100}, 1);
    test_verify(&t, "C", (rect_t){200, 0, 100, 100}, 2);
    test_verify(&t, "D", (rect_t){300, 0, 100, 100}, 3);

    test_deinit(&t);
}

static void test_line_scale_x_inverse()
{
    struct test t;
    test_init(&t, __FUNCTION__);

    test_add_output(&t, "D", (rect_t){150, 0, 50, 50}, 100, 100);
    test_add_output(&t, "C", (rect_t){100, 0, 50, 50}, 100, 100);
    test_add_output(&t, "B", (rect_t){50, 0, 50, 50}, 100, 100);
    test_add_output(&t, "A", (rect_t){0, 0, 50, 50}, 100, 100);

    test_verify(&t, "A", (rect_t){0, 0, 100, 100}, 0);
    test_verify(&t, "B", (rect_t){100, 0, 100, 100}, 1);
    test_verify(&t, "C", (rect_t){200, 0, 100, 100}, 2);
    test_verify(&t, "D", (rect_t){300, 0, 100, 100}, 3);

    test_deinit(&t);
}

static void test_line_scale_mixed()
{
    struct test t;
    test_init(&t, __FUNCTION__);

    test_add_output(&t, "E2", (rect_t){100, 0, 50, 50}, 100, 100);
    test_add_output(&t, "E1", (rect_t){50, 0, 50, 50}, 100, 100);
    test_add_output(&t, "C", (rect_t){0, 0, 50, 50}, 100, 100);
    test_add_output(&t, "W1", (rect_t){-50, 0, 50, 50}, 100, 100);
    test_add_output(&t, "W2", (rect_t){-100, 0, 50, 50}, 100, 100);

    test_verify(&t, "C", (rect_t){0, 0, 100, 100}, 0);
    test_verify(&t, "W2", (rect_t){-200, 0, 100, 100}, 1);
    test_verify(&t, "W1", (rect_t){-100, 0, 100, 100}, 2);
    test_verify(&t, "E1", (rect_t){100, 0, 100, 100}, 3);
    test_verify(&t, "E2", (rect_t){200, 0, 100, 100}, 4);

    test_deinit(&t);
}

static void test_cross_scale()
{
    struct test t;
    test_init(&t, __FUNCTION__);

    test_add_output(&t, "C", (rect_t){0, 0, 50, 50}, 100, 100);
    test_add_output(&t, "E", (rect_t){50, 0, 50, 50}, 100, 100);
    test_add_output(&t, "W", (rect_t){-50, 0, 50, 50}, 100, 100);
    test_add_output(&t, "N", (rect_t){0, -50, 50, 50}, 100, 100);
    test_add_output(&t, "S", (rect_t){0, 50, 50, 50}, 100, 100);

    test_verify(&t, "C", (rect_t){0, 0, 100, 100}, 0);
    test_verify(&t, "W", (rect_t){-100, 0, 100, 100}, 1);
    test_verify(&t, "N", (rect_t){0, -100, 100, 100}, 2);
    test_verify(&t, "S", (rect_t){0, 100, 100, 100}, 3);
    test_verify(&t, "E", (rect_t){100, 0, 100, 100}, 4);

    test_deinit(&t);
}

static void test_exotic()
{
    struct test t;
    test_init(&t, __FUNCTION__);

    test_add_output(&t, "C", (rect_t){0, 0, 50, 50}, 100, 100);
    test_add_output(&t, "E1", (rect_t){50, 0, 25, 25}, 50, 50);
    test_add_output(&t, "E2", (rect_t){50, 25, 25, 25}, 50, 50);
    test_add_output(&t, "S1", (rect_t){0, 50, 25, 25}, 50, 50);
    test_add_output(&t, "S2", (rect_t){25, 50, 25, 25}, 50, 50);

    test_verify(&t, "C", (rect_t){0, 0, 100, 100}, 0);
    test_verify(&t, "S1", (rect_t){0, 100, 50, 50}, 1);
    test_verify(&t, "S2", (rect_t){50, 100, 50, 50}, 2);
    test_verify(&t, "E1", (rect_t){100, 0, 50, 50}, 3);
    test_verify(&t, "E2", (rect_t){100, 50, 50, 50}, 4);

    test_deinit(&t);
}

static void test_exotic_non_anchored()
{
    struct test t;
    test_init(&t, __FUNCTION__);

    test_add_output(&t, "C", (rect_t){0, 0, 50, 50}, 100, 100);
    test_add_output(&t, "E2", (rect_t){50, 25, 25, 25}, 50, 50);
    test_add_output(&t, "S2", (rect_t){25, 50, 25, 25}, 50, 50);

    test_verify(&t, "C", (rect_t){0, 0, 100, 100}, 0);
    test_verify(&t, "S2", (rect_t){50, 100, 50, 50}, 1);
    test_verify(&t, "E2", (rect_t){100, 50, 50, 50}, 2);

    test_deinit(&t);
}

static void test_exotic_non_anchored1()
{
    struct test t;
    test_init(&t, __FUNCTION__);

    test_add_output(&t, "C", (rect_t){0, 0, 100, 100}, 100, 100);
    test_add_output(&t, "E2", (rect_t){100, 50, 25, 25}, 50, 50);
    test_add_output(&t, "S2", (rect_t){50, 100, 25, 25}, 50, 50);

    test_verify(&t, "C", (rect_t){0, 0, 100, 100}, 0);
    test_verify(&t, "S2", (rect_t){50, 100, 50, 50}, 1);
    test_verify(&t, "E2", (rect_t){100, 50, 50, 50}, 2);

    test_deinit(&t);
}

static void test_exotic_non_anchored2()
{
    struct test t;
    test_init(&t, __FUNCTION__);

    test_add_output(&t, "C", (rect_t){0, 0, 50, 50}, 100, 100);
    test_add_output(&t, "E2", (rect_t){50, 25, 25, 25}, 50, 50);
    test_add_output(&t, "S2", (rect_t){25, 50, 25, 25}, 50, 50);
    test_add_output(&t, "N2", (rect_t){25, -25, 25, 25}, 50, 50);
    test_add_output(&t, "W2", (rect_t){-25, 25, 25, 25}, 50, 50);
    test_add_output(&t, "NW", (rect_t){-25, -25, 25, 25}, 50, 50);

    test_verify(&t, "C", (rect_t){0, 0, 100, 100}, 0);
    test_verify(&t, "NW", (rect_t){-50, -50, 50, 50}, 1);
    test_verify(&t, "W2", (rect_t){-50, 50, 50, 50}, 2);
    test_verify(&t, "N2", (rect_t){50, -50, 50, 50}, 3);
    test_verify(&t, "S2", (rect_t){50, 100, 50, 50}, 4);
    test_verify(&t, "E2", (rect_t){100, 50, 50, 50}, 5);

    test_deinit(&t);
}

static void test_exotic_non_anchored3()
{
    struct test t;
    test_init(&t, __FUNCTION__);

    test_add_output(&t, "C", (rect_t){0, 0, 50, 50}, 100, 100);
    test_add_output(&t, "NE", (rect_t){50, -25, 50, 50}, 100, 100);
    test_add_output(&t, "NW", (rect_t){-50, -25, 50, 50}, 100, 100);
    test_add_output(&t, "SW", (rect_t){-50, 25, 50, 50}, 100, 100);
    test_add_output(&t, "SE", (rect_t){50, 25, 50, 50}, 100, 100);

    test_verify(&t, "C", (rect_t){0, 0, 100, 100}, 0);
    test_verify(&t, "NW", (rect_t){-100, -50, 100, 100}, 1);
    test_verify(&t, "SW", (rect_t){-100, 50, 100, 100}, 2);
    test_verify(&t, "NE", (rect_t){100, -50, 100, 100}, 3);
    test_verify(&t, "SE", (rect_t){100, 50, 100, 100}, 4);

    test_deinit(&t);
}

static void test_exotic_non_anchored4()
{
    struct test t;
    test_init(&t, __FUNCTION__);

    test_add_output(&t, "C", (rect_t){0, 0, 50, 50}, 100, 100);
    test_add_output(&t, "N", (rect_t){-25, 75, 100, 50}, 200, 100);

    test_verify(&t, "C", (rect_t){0, 0, 100, 100}, 0);
    test_verify(&t, "N", (rect_t){-50, 150, 200, 100}, 1);

    test_deinit(&t);
}

static void test_quadrant_2()
{
    struct test t;
    test_init(&t, __FUNCTION__);

    test_add_output(&t, "A", (rect_t){50, 100, 50, 50}, 100, 100);
    test_add_output(&t, "B", (rect_t){100, 75, 50, 50}, 100, 100);

    test_verify(&t, "A", (rect_t){50, 100, 100, 100}, 0);
    test_verify(&t, "B", (rect_t){150, 50, 100, 100}, 1);

    test_deinit(&t);
}

static void test_overlapped()
{
    struct test t;
    test_init(&t, __FUNCTION__);

    test_add_output(&t, "C1", (rect_t){0, 0, 50, 50}, 100, 100);
    test_add_output(&t, "C2", (rect_t){0, 0, 25, 25}, 50, 50);
    test_add_output(&t, "C3", (rect_t){25, 25, 50, 50}, 100, 100);
    test_add_output(&t, "C4", (rect_t){-25, -25, 50, 50}, 100, 100);

    test_verify(&t, "C1", (rect_t){0, 0, 100, 100}, 0);
    test_verify(&t, "C2", (rect_t){0, 0, 50, 50}, 1);
    test_verify(&t, "C4", (rect_t){-50, -50, 100, 100}, 2);
    test_verify(&t, "C3", (rect_t){50, 50, 100, 100}, 3);

    test_deinit(&t);
}

int main()
{
    test_line_no_scale();
    test_line_scale();
    test_line_scale1();
    test_line_scale_x();
    test_line_scale_x_inverse();
    test_line_scale_mixed();
    test_cross_scale();
    test_exotic();
    test_exotic_non_anchored();
    test_exotic_non_anchored1();
    test_exotic_non_anchored2();
    test_exotic_non_anchored3();
    test_exotic_non_anchored4();
    test_quadrant_2();
    test_overlapped();
    fprintf(stderr, "=== %s ===\n", !has_error ? "SUCCESS" : "FAIL");
}

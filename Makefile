test: wine-wayland-outputs
	./wine-wayland-outputs

wine-wayland-outputs: wine-wayland-outputs.c
	gcc -fsanitize=address -g -o wine-wayland-outputs wine-wayland-outputs.c -lwayland-client
